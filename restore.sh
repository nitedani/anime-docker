fileid="1wBbl8lPo4UdBf2ArCWAwOeqPbijI33SS"
filename="latest.tar.gz"
curl -c /data/db/cookie -s -L "https://drive.google.com/uc?export=download&id=${fileid}" >/dev/null
curl -Lb /data/db/cookie "https://drive.google.com/uc?export=download&confirm=$(awk '/download/ {print $NF}' /data/db/cookie)&id=${fileid}" -o /data/db/${filename}
tar -C /data/db --strip-components=1 -xf /data/db/${filename}
rm /data/db/${filename}
rm /data/db/cookie