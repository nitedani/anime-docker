version: "3.8"

services:
  jikan-rest-api:
    env_file: .jikan.env
    build:
      context: jikan
      dockerfile: Dockerfile
    depends_on:
      - jikan-redis
    restart: always

  jikan-redis:
    env_file: .jikan.env
    image: redis
    restart: always

  web-app:
    env_file: .web-app.env
    volumes:
      - subtitles:/app/temp/subs
      - torrents:/app/temp/torrent-stream
    build:
      context: web-app
      dockerfile: Dockerfile
    depends_on:
      - mongodb
    ports:
      - 4200:4200
    restart: always

  mongodb:
    env_file: .mongodb.env
    image: mongo
    volumes:
      - ./backups:/backups:ro
      - database:/data/db
      - ./restore.sh:/docker-entrypoint-initdb.d/restore.sh
    restart: always
    ports:
      - 9009:27017

  backup:
    environment:
      BACKUP_FILENAME: latest.tar.gz
    image: futurice/docker-volume-backup
    volumes:
      - database:/backup:ro
      - ./backups:/archive

volumes:
  database:
  subtitles:
  torrents:
